#!/usr/bin/env python3.11
# -*- coding: utf-8 -*-


import unittest

import fill3
import fill3.terminal as terminal
import termstr


class WidgetTests(unittest.TestCase):

    TEXT_A = fill3.Text("A")
    TEXT_B = fill3.Text("B")

    def assert_string(self, appearance, expected_string):
        self.assertEqual(str(termstr.join("\n", appearance)), expected_string)

    def test_rows_widget(self):
        rows = fill3.Row([self.TEXT_A, self.TEXT_B])
        self.assert_string(rows.appearance(), "AB")
        rows = fill3.Row([fill3.Filler(self.TEXT_A), fill3.Filler(self.TEXT_B)])
        self.assert_string(rows.appearance_for((4, 1)), "A B ")

    def test_columns_widget(self):
        columns = fill3.Column([self.TEXT_A, self.TEXT_B])
        self.assert_string(columns.appearance(), "A\n"
                                                 "B")

    def test_text_widget(self):
        self.assert_string(self.TEXT_A.appearance(), "A")
        text = "foo\nbar"
        self.assert_string(fill3.Text(text).appearance(), "foo\n"
                                                          "bar")

    def test_portal_widget(self):
        row = fill3.Row([fill3.Text("foo"), fill3.Text("bar")])
        portal = fill3.Portal(row, (1, 0))
        self.assert_string(portal.appearance_for((5, 1)), "oobar")
        portal.position = (0, 10)
        self.assert_string(portal.appearance_for((1, 1)), " ")
        portal.position = (1, 0)
        self.assert_string(portal.appearance_for((6, 2)), "oobar \n      ")
        portal.position = (-1, -1)
        self.assert_string(portal.appearance_for((2, 2)), "  \n f")
        self.assert_string(portal.appearance_for((8, 3)), "        \n foobar \n        ")
        portal.position = (5, 0)
        self.assert_string(portal.appearance_for((2, 1)), "r ")
        # limit scroll
        portal.position = (0, 0)
        portal.limit_scroll((1, 1), (1, 1))
        self.assertEqual(portal.position, (0, 0))
        portal.position = (-1, 0)
        portal.limit_scroll((1, 1), (1, 1))
        self.assertEqual(portal.position, (0, 0))
        portal.position = (1, 0)
        portal.limit_scroll((6, 1), (1, 1))
        self.assertEqual(portal.position, (0, 0))
        portal.x_alignment = fill3.Alignment.right
        portal.limit_scroll((6, 1), (1, 1))
        self.assertEqual(portal.position, (-5, 0))

    def test_border_widget(self):
        contents = fill3.Filler(self.TEXT_A)
        self.assert_string(fill3.Border(contents).appearance_for((3, 3)), "┌─┐\n"
                                                                          "│A│\n"
                                                                          "└─┘")
        for empty_contents in [fill3.Filler(fill3.Text("")), fill3.Column([])]:
            self.assert_string(fill3.Border(empty_contents).appearance_for((2, 2)), "┌┐\n"
                                                                                    "└┘")
        self.assert_string(fill3.Border(fill3.Column([])).appearance(), "┌┐\n"
                                                                        "└┘")
        self.assert_string(fill3.Border(empty_contents).appearance_for((3, 3)), "┌─┐\n"
                                                                                "│ │\n"
                                                                                "└─┘")
        text = fill3.Text("abcdef")
        self.assert_string(fill3.Border(text, title="AB").appearance_for((8, 3)), "┌─ AB ─┐\n"
                                                                                  "│abcdef│\n"
                                                                                  "└──────┘")
        self.assert_string(fill3.Border(text, title="ABC").appearance_for((6, 3)), "┌ …C ┐\n"
                                                                                   "│abcd│\n"
                                                                                   "└────┘")

    def test_placeholder_widget(self):
        placeholder = fill3.Placeholder(self.TEXT_A)
        self.assert_string(placeholder.appearance(), "A")
        placeholder.widget = self.TEXT_B
        self.assert_string(placeholder.appearance(), "B")

    def assert_string2(self, appearance, expected_string):
        self.assertEqual(("\n".join(line.data for line in appearance),
             "".join("i" if style.fg_color == fill3.ScrollBar.DEFAULT_BACKGROUND_COLOR else " "
                     for line in appearance for style in line.style)), expected_string)

    def test_scroll_bar(self):
        scroll_bar = fill3.ScrollBar(is_horizontal=True)
        self.assertEqual(scroll_bar.interval, (0, 0))
        self.assert_string2(scroll_bar.appearance_for((1, 1)), (" ", "i"))
        scroll_bar.interval = (0, 0.5)
        self.assert_string2(scroll_bar.appearance_for((2, 1)), ("  ", "i "))
        scroll_bar.interval = (0, 0.1)
        self.assert_string2(scroll_bar.appearance_for((2, 1)), ("  ", "i "))
        scroll_bar.interval = (0.25, 0.75)
        self.assert_string2(scroll_bar.appearance_for((4, 1)), ("  █ ", " i  "))
        scroll_bar.interval = (0, 0.75)
        self.assert_string2(scroll_bar.appearance_for((2, 1)), (" ▌", "i "))
        scroll_bar = fill3.ScrollBar(is_horizontal=False)
        self.assertEqual(scroll_bar.interval, (0, 0))
        self.assert_string2(scroll_bar.appearance_for((1, 1)), ("█", " "))
        scroll_bar.interval = (0, 0.5)
        self.assert_string2(scroll_bar.appearance_for((1, 2)), ("█\n"
                                                                "█", " i"))
        scroll_bar.interval = (0, 0.1)
        self.assert_string2(scroll_bar.appearance_for((1, 2)), ("█\n"
                                                                "█", " i"))
        scroll_bar.interval = (0.25, 0.75)
        self.assert_string2(scroll_bar.appearance_for((1, 4)), (" \n"
                                                                "█\n"
                                                                "█\n"
                                                                "█", "   i"))
        scroll_bar.interval = (0, 0.75)
        self.assert_string2(scroll_bar.appearance_for((1, 2)), ("█\n"
                                                                "▄", " i"))

    def test_table_widget(self):
        table = fill3.Table([])
        self.assert_string(table.appearance(), "")
        table = fill3.Table([[self.TEXT_A]])
        self.assert_string(table.appearance(), "A")
        table = fill3.Table([[self.TEXT_A, self.TEXT_B]])
        self.assert_string(table.appearance(), "AB")
        table = fill3.Table([[self.TEXT_A, self.TEXT_B],
                             [self.TEXT_B, self.TEXT_A]])
        self.assert_string(table.appearance(), "AB\n"
                                               "BA")
        label_foo = fill3.Text("FOO")
        table = fill3.Table([[label_foo, self.TEXT_B],
                             [self.TEXT_B, self.TEXT_A]])
        self.assert_string(table.appearance(), "FOOB\n"
                                               "B  A")


class TerminalInputTestCase(unittest.TestCase):

    def test_input_regex(self):
        self.assertEqual(fill3._INPUT_REGEX.findall(""), [])
        self.assertEqual(fill3._INPUT_REGEX.findall("a"), ["a"])
        self.assertEqual(fill3._INPUT_REGEX.findall("ab"), ["ab"])
        self.assertEqual(fill3._INPUT_REGEX.findall("a\nb"), ["a", "\n", "b"])
        self.assertEqual(fill3._INPUT_REGEX.findall("a\tb"), ["a", "\t", "b"])
        self.assertEqual(fill3._INPUT_REGEX.findall(terminal.UP * 2), [terminal.UP] * 2)
        self.assertEqual(fill3._INPUT_REGEX.findall(terminal.CTRL_C * 2), [terminal.CTRL_C] * 2)


if __name__ == "__main__":
    unittest.main()
