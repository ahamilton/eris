#!/usr/bin/env python3.11


import os
import pickle
import unittest


import termstr


os.environ["TERM"] = "xterm-256color"
ESC = "\x1b"


class XtermColorsTests(unittest.TestCase):

    def test_xterm_colors(self):
        self.assertEqual(len(termstr.XTERM_COLORS), 256)
        self.assertEqual(termstr.XTERM_COLORS[0], (0, 0, 0))
        self.assertEqual(termstr.XTERM_COLORS[100], (135, 135, 0))
        self.assertEqual(termstr.XTERM_COLORS[255], (238, 238, 238))

    def test_closest_color_index(self):
        self.assertEqual(termstr.closest_color_index((0, 0, 0), termstr.XTERM_COLORS), 0)
        self.assertEqual(termstr.closest_color_index((255, 255, 255), termstr.XTERM_COLORS), 15)
        self.assertEqual(termstr.closest_color_index((135, 135, 1), termstr.XTERM_COLORS), 100)


class CharStyleTests(unittest.TestCase):

    def setUp(self):
        self.style = termstr.CharStyle()

    def test_default_char_style(self):
        self.assertEqual(self.style.fg_color, termstr.Color.white)
        self.assertEqual(self.style.bg_color, termstr.Color.black)
        self.assertEqual(self.style.is_bold, False)
        self.assertEqual(self.style.is_underlined, False)

    def test_pickle_char_style(self):
        style = termstr.CharStyle()
        loaded_style = pickle.loads(pickle.dumps(style))
        self.assertEqual(style, loaded_style)
        self.assertTrue(style is loaded_style)

    def test_repr(self):
        self.assertEqual(repr(self.style), "<CharStyle: fg:(255, 255, 255) bg:(0, 0, 0) attr:>")

    def test_code_for_term(self):
        self.assertEqual(self.style.code_for_term, "\x1b[m\x1b[38;2;255;255;255m\x1b[48;2;0;0;0m")


class TermStrTests(unittest.TestCase):

    def test_termstr(self):
        foo = termstr.TermStr("foo")
        foobar = termstr.TermStr("foobar")
        bold_style = termstr.CharStyle(3, 5, is_bold=True)
        foo_bold = termstr.TermStr("foo", bold_style)
        self.assertEqual(repr(foo_bold), "<TermStr: 'foo'>")
        self.assertEqual(foo + "bar", termstr.TermStr("foobar"))
        self.assertEqual(foo + termstr.TermStr("bar"), termstr.TermStr("foobar"))
        self.assertEqual("bar" + foo, termstr.TermStr("barfoo"))
        self.assertFalse(foo == foo_bold)
        self.assertFalse(foo_bold == foo)
        self.assertFalse("foo" == foo_bold)
        self.assertTrue("food" != foo_bold)
        self.assertFalse(foo != foo)
        self.assertTrue(foo != foo_bold)
        self.assertFalse(foo_bold == "foo")
        self.assertTrue(foo_bold != "food")
        self.assertEqual(foobar[:2], termstr.TermStr("fo"))
        self.assertEqual(foobar[2:], termstr.TermStr("obar"))
        self.assertEqual(foobar[::2], termstr.TermStr("foa"))
        self.assertEqual(foobar[3], termstr.TermStr("b"))
        self.assertEqual(foo_bold[1], termstr.TermStr("o", bold_style))
        self.assertTrue(foo.startswith("fo"))
        self.assertTrue(foo.endswith("oo"))
        self.assertEqual(foo.index("o"), 1)
        self.assertTrue("fo" in foo)
        self.assertEqual(foo.find("oo"), 1)
        self.assertEqual(termstr.TermStr("fo") * 2, termstr.TermStr("fofo"))
        self.assertEqual(2 * termstr.TermStr("fo"), termstr.TermStr("fofo"))
        self.assertEqual(foobar.split("b"), [termstr.TermStr("foo"), termstr.TermStr("ar")])
        self.assertEqual(foo.join(["C", "D"]), termstr.TermStr("CfooD"))
        self.assertEqual(foo.join(["C", termstr.TermStr("D")]), termstr.TermStr("CfooD"))
        self.assertEqual(foo.join([]), termstr.TermStr(""))
        self.assertEqual(foo.join(["C"]), termstr.TermStr("C"))
        bar = termstr.TermStr("bar", bold_style)
        self.assertEqual((foo + "\n" + bar).splitlines(), [foo, bar])
        self.assertEqual((foo + "\r\n" + bar).splitlines(), [foo, bar])
        self.assertEqual((foo + "\n" + bar).splitlines(keepends=True),
                         [termstr.TermStr("foo\n"), bar])
        self.assertEqual((foo + "\r\n" + bar).splitlines(keepends=True),
                         [termstr.TermStr("foo\r\n"), bar])
        self.assertEqual(foo.ljust(5), foo + termstr.TermStr("  "))
        self.assertEqual(foo.rjust(5), termstr.TermStr("  ") + foo)
        self.assertEqual(termstr.TermStr("FOO").lower(), foo)
        self.assertEqual(termstr.TermStr("FOO", bold_style).lower(), foo_bold)
        self.assertEqual(termstr.TermStr("FOO").swapcase(), foo)
        self.assertEqual(termstr.TermStr("FOO", bold_style).swapcase(), foo_bold)
        phrase = termstr.TermStr("foo bar")
        self.assertEqual(phrase.title(), termstr.TermStr("Foo Bar"))
        self.assertEqual(phrase.capitalize(), termstr.TermStr("Foo bar"))
        self.assertEqual(foo.upper(), termstr.TermStr("FOO"))
        self.assertEqual(foo_bold.center(0), foo_bold)
        self.assertEqual(foo_bold.center(7),
                         termstr.TermStr("  ") + foo_bold + termstr.TermStr("  "))
        self.assertEqual(foo_bold.ljust(0), foo_bold)
        self.assertEqual(foo_bold.ljust(5), foo_bold + termstr.TermStr("  "))
        self.assertEqual(foo_bold.rjust(0), foo_bold)
        self.assertEqual(foo_bold.rjust(5), termstr.TermStr("  ") + foo_bold)
        baz = termstr.TermStr("b👋z")
        self.assertEqual(len(baz), 4)
        self.assertEqual(baz[3:], termstr.TermStr("z"))
        self.assertEqual(baz[:2], termstr.TermStr("b "))
        self.assertEqual(baz[2:], termstr.TermStr(" z"))
        baz = termstr.TermStr("b⭐" + chr(65039) + "z")
        self.assertEqual(len(baz), 4)
        self.assertEqual(baz[3:], termstr.TermStr("z"))
        self.assertEqual(baz[:2], termstr.TermStr("b "))
        self.assertEqual(baz[2:], termstr.TermStr(" z"))
        baz = termstr.TermStr("⭐⭐")
        self.assertEqual(len(baz), 4)
        self.assertEqual(termstr.TermStr("foo", is_bold=True), termstr.TermStr("foo").bold())

    def test_from_term(self):
        def test_round_trip(term_str):
            self.assertEqual(termstr.TermStr.from_term(str(term_str)), term_str)

        test_round_trip(termstr.TermStr("foo"))
        test_round_trip(termstr.TermStr("foo").bold())
        test_round_trip(termstr.TermStr("foo").underline())
        test_round_trip(termstr.TermStr("foo").italic())
        test_round_trip(termstr.TermStr("foo").fg_color(termstr.Color.red))
        test_round_trip(
            termstr.TermStr("foo").fg_color(termstr.Color.red).bg_color(termstr.Color.green))
        test_round_trip(termstr.TermStr("foo").fg_color(1))
        test_round_trip(termstr.TermStr("foo").bg_color(10))
        self.assertEqual(
            termstr.TermStr.from_term("foo"), termstr.TermStr("foo"))
        self.assertEqual(termstr.TermStr.from_term(ESC + "[33mfoo"),
                         termstr.TermStr("foo").fg_color(3))
        self.assertEqual(termstr.TermStr.from_term(ESC + "[45mfoo"),
                         termstr.TermStr("foo").bg_color(5))
        self.assertEqual(termstr.TermStr.from_term(ESC + "[45mfoo" + ESC + "[mbar"),
                         termstr.TermStr("foo").bg_color(5) + termstr.TermStr("bar"))
        self.assertEqual(termstr.TermStr.from_term(ESC + "[45mfoo" + ESC + "[0mbar"),
                         termstr.TermStr("foo").bg_color(5) + termstr.TermStr("bar"))
        self.assertEqual(termstr.TermStr.from_term(ESC + "[1;3mfoo"),
                         termstr.TermStr("foo").bold().italic())
        self.assertEqual(termstr.TermStr.from_term(ESC + "[01mfoo"),
                         termstr.TermStr("foo").bold())
        self.assertEqual(termstr.TermStr.from_term(ESC + "[Kfoo"),
                         termstr.TermStr("foo"))
        self.assertEqual(termstr.TermStr.from_term(ESC + "[95mfoo"),
                         termstr.TermStr("foo").fg_color(13))
        self.assertEqual(termstr.TermStr.from_term(ESC + "[105mfoo"),
                         termstr.TermStr("foo").bg_color(13))
        self.assertEqual(termstr.TermStr.from_term(ESC + "(B" + ESC + "[mfoo"),
                         termstr.TermStr("foo"))
        self.assertEqual(termstr.TermStr.from_term(ESC + "39;49;00mfoo"), termstr.TermStr("foo"))


if __name__ == "__main__":
    unittest.main()
