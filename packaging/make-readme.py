#!/usr/bin/env python3.11
# -*- coding: utf-8 -*-


import eris.tools as tools


def main():
    git_tools = [tools.git_diff, tools.git_blame, tools.git_log]
    all_tools = [(["*"], tools.generic_tools() + git_tools)] + tools.TOOLS_FOR_EXTENSIONS
    tool_set = set()
    extension_set = set()
    for extensions, tools_ in all_tools:
        tool_set.update(tools_)
        extension_set.update(extensions)
    print(f"""\
# Eris Codebase Monitor

## Summary

Eris maintains an up-to-date set of reports for every file in a codebase.

## Installation

#### Debian / Ubuntu / Fedora / Arch / Alpine

Install eris with pipx, then install all the tools eris uses:

    pipx install --system-site-packages git+https://gitlab.com/ahamilton/eris@v2024.12.28#subdirectory=eris
    eris --install-all-tools

Or install from source: (includes tools)

    git clone https://gitlab.com/ahamilton/eris
    cd eris
    ./install

Then to run:

    eris
    eris-webserver <project>  # Or a simple web interface.

## Tools

File types({len(extension_set)-1}) | Tools({len(tool_set)})
----------:| -----""")
    for extensions, tools_ in all_tools:
        print("%s | %s" % (" ".join("." + extension for extension in extensions),
                           " • ".join(f"[{tool.__name__}]({tool.url})" for tool in tools_)))


if __name__ == "__main__":
    main()
