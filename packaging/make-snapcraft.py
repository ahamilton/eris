#!/usr/bin/env python3.11
# -*- coding: utf-8 -*-


import textwrap

import eris.__main__
import eris.tools as tools


def main():
    lines = eris.__main__.__doc__.splitlines()
    summary = lines[2]
    description = textwrap.indent("\n".join(lines[2:]), "  ")
    print(f"""\
name: eris
base: core20
version: 2021.12.06
summary: {summary}
description: |
{description}

grade: devel
confinement: strict

apps:
  eris:
    command: bin/eris
    environment:
      PYTHONPATH: $SNAP/lib/python3.11/site-packages:$SNAP/lib/python3.11/dist-packages:$PYTHONPATH
    plugs:
      - home
  webserver:
    command: bin/eris-webserver
    environment:
      PYTHONPATH: $SNAP/lib/python3.11/site-packages:$SNAP/lib/python3.11/dist-packages:$PYTHONPATH
    plugs:
      - home
      - network-bind

passthrough:
  layout:
    $SNAP/usr/bin/php:
      symlink: $SNAP/usr/bin/php7.4

parts:
  eris:
    plugin: python
    source: .
    python-packages:
      - ./termstr
      - ./lscolors
      - ./fill3
      - ./eris
    build-environment:
      - SNAPCRAFT_PYTHON_INTERPRETER: python3.11
    stage-packages:
      - python3.11-venv
      - python3-distutils
      - python3-pkg-resources
      - """, end="")
    print("\n      - ".join(sorted(tools.dependencies())))


if __name__ == "__main__":
    main()
