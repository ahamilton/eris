#!/usr/bin/env python3.11


import asyncio
import functools
import gzip
import http.server
import os
import pickle
import sys
import urllib.parse

import fill3
import termstr

import eris.tools as tools


USAGE = """Usage:
  eris-webserver <directory>

Example:
  eris-webserver my_project
"""


def make_page(body_html, title):
    text = f"""
<html>
    <head>
        <title>{title}</title>
        <style type="text/css">
            body {{ margin:2; background-color:black; }}
            iframe {{ height:100%;width:100%;border:1px solid white; }}
            a:focus, a:hover {{ filter:brightness(180%); }}
        </style>
    </head>
    <body>{body_html}</body>
</html>"""
    return gzip.compress(text.encode("utf-8"))


@functools.lru_cache(maxsize=100)
def make_listing_page(url_path):
    unquoted_path = urllib.parse.unquote(url_path)
    path, tool_name = os.path.split(unquoted_path)
    result = index[(path, tool_name)]
    tool = getattr(tools, tool_name)
    tool_name_colored = tools.tool_name_colored(tool, path)
    header = fill3.appearance_as_html([lscolors.path_colored(path) + " - " + tool_name_colored,
                                       termstr.TermStr(" " * 100, is_underlined=True)])
    body = fill3.appearance_as_html(result.appearance())
    return make_page(header + body, f"{path} - {tool_name}")


class Webserver(http.server.BaseHTTPRequestHandler):

    def _set_headers(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.send_header("Content-encoding", "gzip")
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        if self.path == "/":
            page = main_page
        elif self.path == "/summary":
            page = summary_page
        elif "/" in self.path[1:]:
            page = make_listing_page(self.path[1:])
        else:
            return
        self.wfile.write(page)


def make_main_page(project_name):
    body_html = """
<table style="height:100%;width:100%;">
    <tr>
        <td style="height:100%;width:38.198%;">
            <iframe name=summary src="summary"></iframe></td>
        <td style="height:100%;">
            <iframe name=listing></iframe></td>
    </tr>
</table>"""
    return make_page(body_html, "Summary of " + project_name)


def make_summary_page(project_name, summary):
    summary_html, summary_styles = summary.as_html()
    body_html = "\n".join(style.as_html() for style in summary_styles) + "\n" + summary_html
    return make_page(body_html, "Summary of " + project_name)


def run(server_class=http.server.HTTPServer, handler_class=Webserver, port=8080):
    server_address = ("", port)
    httpd = server_class(server_address, handler_class)
    print(f"Starting httpd…  See http://localhost:{port}/")
    httpd.serve_forever()


def get_summary(project_path):
    pickle_path = os.path.join(project_path, tools.CACHE_PATH, "summary.pickle")
    with gzip.open(pickle_path, "rb") as file_:
        screen = pickle.load(file_)
    summary = screen._summary
    summary._jobs_added_event = asyncio.Event()
    for entry in summary._old_entries:
        summary.add_entry(entry)
    if summary.is_directory_sort:
        summary.is_directory_sort = False
        summary.sort_entries()
    return summary


def main():
    global main_page, summary_page, index
    if len(sys.argv) == 1:
        print(USAGE)
        sys.exit(1)
    project_path = os.path.abspath(sys.argv[1])
    os.chdir(project_path)
    summary = get_summary(project_path)
    project_name = os.path.basename(project_path)
    summary_page = make_summary_page(project_name, summary)
    index = {}
    for row in summary._entries:
        for result in row:
            index[(result.path[2:], result.tool.__name__)] = result.result
    main_page = make_main_page(project_name)
    run()


if __name__ == "__main__":
    main()
