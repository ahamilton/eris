#!/usr/bin/env python3.11


import pydoc
import sys

import termstr


class TermDoc(pydoc.TextDoc):

    def bold(self, text):
        return str(termstr.TermStr(text, is_bold=True))


def main():
    path = sys.argv[1]
    try:
        print(pydoc.render_doc(pydoc.importfile(path), renderer=TermDoc()))
    except pydoc.ErrorDuringImport as e:
        print(e)
        return 1


if __name__ == "__main__":
    main()
