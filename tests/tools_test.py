#!/usr/bin/env python3.11


import contextlib
import os
import shutil
import unittest
import unittest.mock

import termstr

import golden
import eris.tools as tools


os.environ["TERM"] = "xterm-256color"
os.environ["TZ"] = "GMT"
ERIS_ROOT = os.path.dirname(__file__)


class ExecutablesTestCase(unittest.TestCase):

    def test_executables_exist_in_path(self):
        # Tools not in ubuntu:
        exceptions = {tools.wasm_validate, tools.wasm_objdump}
        for tool in tools.tools_all() - exceptions:
            if hasattr(tool, "executables"):
                for executable in tool.executables:
                    with self.subTest(executable=executable, tool=tool):
                        self.assertTrue(shutil.which(executable))


def widget_to_string(widget):
    return str(termstr.join("\n", widget.appearance()))


@contextlib.contextmanager
def chdir(path):
    old_cwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(old_cwd)


def result_path(tool, input_filename):
    filename = tool.__name__ + "-" + input_filename.replace(".", "_")
    return os.path.join(ERIS_ROOT, "golden-files", "results", filename)


def run_tool(tool, input_filename):
    with chdir(os.path.join(ERIS_ROOT, "golden-files")):
        return tool(os.path.join(".", "input", input_filename))


class ToolsTestCase(unittest.TestCase):

    def _test_tool(self, tool, sub_tests):
        for input_filename, expected_status in sub_tests:
            with self.subTest(input_filename=input_filename):
                status, result = run_tool(tool, input_filename)
                golden_path = result_path(tool, input_filename)
                golden.assertGolden(str(result), golden_path)
                self.assertEqual(status, expected_status)

    def test_metadata(self):
        mock_stat_result = unittest.mock.Mock(
            st_mode=0o755, st_mtime=1454282045, st_ctime=1454282045, st_atime=1454282047,
            st_size=12, st_uid=1111, st_gid=1111, st_nlink=2)
        mock_pw_entry = unittest.mock.Mock(pw_name="foo")
        with unittest.mock.patch.object(os, "stat", return_value=mock_stat_result):
            with unittest.mock.patch.object(tools.pwd, "getpwuid", return_value=mock_pw_entry):
                self._test_tool(tools.metadata, [("hi3.py", tools.Status.ok)])

    def test_contents(self):
        self._test_tool(tools.contents, [("hi3.py", tools.Status.ok)])

    HI_OK = [("hi3.py", tools.Status.ok)]

    def test_python_syntax(self):
        self._test_tool(tools.python_syntax, self.HI_OK)

    HI_OK = [("hi3.py", tools.Status.ok)]

    def test_mypy(self):
        self._test_tool(tools.mypy, self.HI_OK)

    def test_pycodestyle(self):
        self._test_tool(tools.pycodestyle, self.HI_OK)

    def test_pyflakes(self):
        self._test_tool(tools.pyflakes, self.HI_OK)

    def test_pylint(self):
        self._test_tool(tools.pylint, self.HI_OK)

    def test_python_gut(self):
        self._test_tool(tools.python_gut, self.HI_OK)

    def test_python_mccabe(self):
        self._test_tool(tools.python_mccabe, self.HI_OK)

    def test_perl_syntax(self):
        self._test_tool(tools.perl_syntax, [("perl.pl", tools.Status.ok)])

    def test_c_syntax_gcc(self):
        self._test_tool(tools.c_syntax_gcc, [("hello.c", tools.Status.ok)])

    def test_objdump_headers(self):
        self._test_tool(tools.objdump_headers, [("rotatingtree.o", tools.Status.ok)])

    def test_objdump_disassemble(self):
        self._test_tool(tools.objdump_disassemble, [("rotatingtree.o", tools.Status.problem)])

    def test_readelf(self):
        self._test_tool(tools.readelf, [("rotatingtree.o", tools.Status.ok)])

    def test_zipinfo(self):
        self._test_tool(tools.zipinfo, [("hi.zip", tools.Status.ok)])

    def test_nm(self):
        self._test_tool(tools.nm, [("libieee.a", tools.Status.ok),
                                   ("libpcprofile.so", tools.Status.ok)])

    def test_pdf2txt(self):
        self._test_tool(tools.pdf2txt, [("standard.pdf", tools.Status.ok)])

    def test_html_syntax(self):
        self._test_tool(tools.html_syntax, [("hi.html", tools.Status.problem)])

    def test_html2text(self):
        self._test_tool(tools.html2text, [("hi.html", tools.Status.ok)])

    def test_cpp_syntax_gcc(self):
        self._test_tool(tools.cpp_syntax_gcc, [("hello.cpp", tools.Status.ok)])

    def test_php8_syntax(self):
        self._test_tool(tools.php8_syntax, [("root.php", tools.Status.ok)])

    def test_pil(self):
        for extension in ["png", "jpg", "gif", "bmp", "ppm", "tiff", "tga"]:
            self._test_tool(tools.pil, [("circle." + extension, tools.Status.ok)])


class LruCacheWithEvictionTestCase(unittest.TestCase):

    def _assert_cache(self, func, hits, misses, current_size):
        cache_info = func.cache_info()
        self.assertEqual(cache_info.hits, hits)
        self.assertEqual(cache_info.misses, misses)
        self.assertEqual(cache_info.currsize, current_size)

    def test_lru_cache_with_eviction(self):
        @tools.lru_cache_with_eviction()
        def a(foo):
            return foo
        self._assert_cache(a, 0, 0, 0)
        self.assertEqual(a(1), 1)
        self._assert_cache(a, 0, 1, 1)
        a(1)
        self._assert_cache(a, 1, 1, 1)
        a.evict(1)
        self._assert_cache(a, 1, 1, 1)
        a(1)
        self._assert_cache(a, 1, 2, 2)


if __name__ == "__main__":
    golden.main()
